package com.example.geometry;

public class LotsOfShapes {

    public static void main(String[] args) {

        Shape[] shapes = new Shape[5];

        shapes[0] = new Rectangle(3, 5);
        shapes[1] = new Rectangle(1, 3);
        shapes[2] = new Circle(5);
        shapes[3] = new Circle(10);
        shapes[4] = new Square(5);

        int count = 1;
        for (Shape s : shapes) {
            System.out.println("Shape #" + count);
            System.out.println("Area: "+s.getArea());
            System.out.println("Perimeter: "+s.getPerimeter());
            System.out.println();
            count++;
        }
    }
}
